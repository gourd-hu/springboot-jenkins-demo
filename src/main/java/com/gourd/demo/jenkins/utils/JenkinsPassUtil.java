package com.gourd.demo.jenkins.utils;

import org.mindrot.jbcrypt.BCrypt;

/**
 * @Description jenkins密码生成
 * @Author gourd.hu
 * @Date 2020/8/7 17:36
 * @Version 1.0
 */
public class JenkinsPassUtil {

    /**
     * 明文
     */
    public static final String GOURD = "gourd";

    public static void main(String[] args)
    {
        // 加密
        String hashPassword = BCrypt.hashpw(GOURD, BCrypt.gensalt());
        System.out.println("生成的密文： "+ hashPassword);

        // 验证匹配
        if (BCrypt.checkpw(GOURD, hashPassword)) {
            System.out.println("匹配");
        } else {
            System.out.println("不匹配");
        }
    }
}
