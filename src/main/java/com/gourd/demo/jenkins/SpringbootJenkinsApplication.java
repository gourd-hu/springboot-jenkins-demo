package com.gourd.demo.jenkins;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * jenkins-demo启动类
 *
 * @author gourd.hu
 */
@SpringBootApplication
public class SpringbootJenkinsApplication {

    private static final Logger logger = LoggerFactory.getLogger(SpringbootJenkinsApplication.class);

    public static void main(String[] args) throws UnknownHostException {

        // new application
        ConfigurableApplicationContext application = new SpringApplicationBuilder()
                .sources(SpringbootJenkinsApplication.class)
                // default properties
                .properties("--spring.profiles.active=test")
                .web(WebApplicationType.SERVLET)
                .run(args);
        logger.warn(">o< 服务启动成功！温馨提示：代码千万行，注释第一行，命名不规范，同事泪两行 >o<");
        Environment env = application.getEnvironment();
        // 是否启用https
        logger.info("Swagger文档: \t\thttp://{}:{}/doc.html", InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"));
    }

}
