package com.gourd.demo.jenkins.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试
 * @author gourd.hu
 */
@Api(tags = "项目测试API", description = "项目测试API" )
@RestController
@RequestMapping("/test")
public class TestController {

    @Value("${env.test}")
    private String envTest;

    @GetMapping("/hello")
    @ApiOperation(value = "测试")
    public String helloTest() {
        return "hello gourd.hu: "+ envTest;
    }
}
