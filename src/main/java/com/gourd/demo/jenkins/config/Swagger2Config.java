package com.gourd.demo.jenkins.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger接口文档
 * 访问：http://localhost:10001/doc.html#/
 *
 * @author gourd.hu
 */
@EnableSwagger2
@EnableKnife4j
@Configuration
public class Swagger2Config {
    @Bean
    public Docket gourdHuDemo() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("接口文档")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gourd.demo.jenkins.controller"))
                .paths(PathSelectors.any()).build();
    }

    public static ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("springboot-jenkins")
                .description("springboot-jenkins是测试自动化发布demo项目")
                .termsOfServiceUrl("https://blog.csdn.net/HXNLYW")
                .contact(new Contact("葫芦胡 ", "https://blog.csdn.net/HXNLYW", "13584278267@163.com"))
                .version("1.0.0").build();
    }
}