#/bin/bash
# 项目参数
PROJECT_NAME=$1
IMAGE_NAME=$2
ENV=$3
SERVER_PORT=$4
echo "PROJECT_NAME: $1 , IMAGE_NAME: $2 , ENV:$3 , SERVER_PORT: $4 "

# 镜像库
REGISTRY_URL=47.103.5.190:80
REGISTRY_USER=admin
REGISTRY_PASS=Hardor12345

# 拉取远程仓库镜像
docker login --username=$REGISTRY_USER --password=$REGISTRY_PASS $REGISTRY_URL
docker pull $REGISTRY_URL/$PROJECT_NAME/$IMAGE_NAME

# JVM参数，DockerFile中接收
JAVA_OPTS="-server -Xms128m -Xmx512m -Xmn256m -XX:MetaspaceSize=128m"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:/data/$PROJECT_NAME/logs/$PROJECT_NAME-gc.log"
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/data/$PROJECT_NAME/logs/$PROJECT_NAME-heap.hprof"
JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=$ENV"
echo "JVM参数: $JAVA_OPTS "

### 发布
# 停止原容器服务
docker rm -f $PROJECT_NAME
echo $?
# 启动容器
docker run -d --name=$PROJECT_NAME -p $SERVER_PORT:$SERVER_PORT --restart=always -e TZ='Asia/Shanghai' -e JAVA_OPTS="$JAVA_OPTS" -e CONTAINER_JAR="$PROJECT_NAME.jar" -v /data/$PROJECT_NAME/logs:/data/$PROJECT_NAME/logs $REGISTRY_URL/$PROJECT_NAME/$IMAGE_NAME
docker image prune -a -f --filter 'until=1h'

# 获取此次启动容器id
CONTAINER_ID=$(docker ps --filter "name=$PROJECT_NAME" -q)
# 后台启动执行停止 docker logs 日志输出
sh $(dirname $(readlink -f $0))/kill_tailf.sh $PROJECT_NAME $CONTAINER_ID &
# 日志滚动输出，此方法是阻塞的
docker logs -f $CONTAINER_ID
echo "[tags] 日志已停止输出"

## 从日志中判断服务是否启动成功或异常
SUM=`docker logs $CONTAINER_ID |grep 'JVM running for' |wc -l`
ERROR_SUM=`docker logs $CONTAINER_ID |grep 'ERROR [           main]' |wc -l`

## 判断部署成功失败
if [ $ERROR_SUM -gt 0 ];then
       echo "[tags] ------------"
       echo "[tags] 本次部署失败"
       echo "[tags] ------------"
       exit 1
fi
if [ $SUM -gt 0 ];then
       echo "[tags] ------------"
       echo "[tags] 本次部署成功"
       echo "[tags] ------------"
else
       echo "[tags] ------------"
       echo "[tags] 本次部署失败"
       echo "[tags] ------------"
       exit 1
fi