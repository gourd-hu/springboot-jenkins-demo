#/bin/bash
# 获取参数
PROJECT_NAME=$1
CONTAINER_ID=$2
echo $PROJECT_NAME $CONTAINER_ID

# 死循环获取日志判断服务启动成功或是异常
# 成功或是异常退出循环，杀死日志输出进程
SECOND=0
while true
do
    SUM=`docker logs $CONTAINER_ID |grep 'JVM running for' |wc -l`
    ERROR_SUM=`docker logs $CONTAINER_ID |grep 'ERROR [           main]' |wc -l`
    # 5分钟超时
    if [ $SECOND -ge 300  ];then
       echo "项目部署超时"
       ps -ef |grep "docker logs -f $CONTAINER_ID" |grep -v grep|awk '{print $2}' |xargs kill
       break
    fi
    if [ $SUM -gt 0 ];then
       sleep 2
       ps -ef |grep "docker logs -f $CONTAINER_ID" |grep -v grep|awk '{print $2}' |xargs kill
       break
    fi
    if [ $ERROR_SUM -gt 0 ];then
       sleep 2
       ps -ef |grep "docker logs -f $CONTAINER_ID" |grep -v grep|awk '{print $2}' |xargs kill
       echo "[tags] 项目启动异常"
       break
    fi
    sleep 1
    SECOND=$((SECOND + 1))
done